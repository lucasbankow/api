﻿using System;
namespace SalesManager.Domain
{
    public class Sales
    {
        public Sales()
        {
            this.AcquireDate = DateTime.Now;
        }
        public int Id { get; set; }
        public string Classification { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public DateTime AcquireDate { get; set; }
        public int UserId { get; set; }

        public string UserSeller { get; set; }
        public override string ToString()
        {
            return this.Classification;
        }
    }
    
}
