﻿namespace SalesManager.Domain
{
    public class User
    {
        public int Id  { get; set; }
        public string Seller { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Administrator { get; set; }
        public string Role { get; set; }

        public override string ToString()
        {
            return this.Seller;
        }

    }
}