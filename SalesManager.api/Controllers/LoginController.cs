﻿using SalesManager.Domain;
using SalesManager.infra.DataContexts;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.UI.WebControls;

namespace SalesManager.api.Controllers
{
    [RoutePrefix("api")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ManagerController : ApiController
    {
        private readonly SalesManangerDataContext db = new SalesManangerDataContext();
        [HttpGet]
        [Route("login/{email}/{password}")]
        public HttpResponseMessage Login(string email, string password)
        {
            if (email == null || password == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                var user = db.Users.Where(x => x.Email == email);

                password = Encrypt(password);

                var logged = user.Where(x => x.Password == password);

                var result = logged;

                return Request.CreateResponse(HttpStatusCode.OK, result);
            } catch
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
        public string Encrypt(string value)

        {
            MD5 md5Hasher = MD5.Create();
            byte[] encryptedValue = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < encryptedValue.Length; i++)

            {
                strBuilder.Append(encryptedValue[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }
    }
}