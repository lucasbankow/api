﻿using Microsoft.AspNetCore.Mvc;
using SalesManager.Domain;
using SalesManager.infra.DataContexts;
using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using System.Web.Http;
using System.Text;
using System.Security.Cryptography;

namespace SalesManager.api.Controllers
{
    [RoutePrefix("api")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SaleController : ApiController
    {
        private readonly SalesManangerDataContext db = new SalesManangerDataContext();
        [Route("sales/{id}")]
        public HttpResponseMessage GetSales(int id)
        {
            var user = db.Users.Find(id);
            if (user.Administrator == true)
            {
                var result = db.Sales.ToList();
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            else if (user.Administrator == false)
            {
                var result = db.Sales.Where(x => x.UserId == id).ToList();
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [Route("new-sale")]

        public HttpResponseMessage PostSale(Sales sales)
        {
            if (sales == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            try
            {
                db.Sales.Add(sales);
                db.SaveChanges();

                var result = sales;

                return Request.CreateResponse(HttpStatusCode.Created);
            }
            catch (Exception)
            {
                var result = sales;
                return Request.CreateResponse(HttpStatusCode.OK, result);
                throw;
            }
        }

        [HttpGet]
        [Route("signin/{email}/{password}")]
        public HttpResponseMessage Login(string email, string password)
        {
            if (email == null || password == null)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            try
            {
                var user = db.Users.Where(x => x.Email == email);

                password = Encrypt(password);

                var logged = user.Where(x => x.Password == password);

                var result = logged;

                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
        }
        public string Encrypt(string value)

        {
            MD5 md5Hasher = MD5.Create();
            byte[] encryptedValue = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < encryptedValue.Length; i++)

            {
                strBuilder.Append(encryptedValue[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }
    }
}