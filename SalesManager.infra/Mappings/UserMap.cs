﻿using SalesManager.Domain;
using System.Data.Entity.ModelConfiguration;

namespace SalesManager.infra.Mappings
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            ToTable("User");

            HasKey(x => x.Id);
            Property(x => x.Email).HasMaxLength(60).IsRequired();
            Property(x => x.Password).HasMaxLength(128).IsRequired();
            Property(x => x.Seller).HasMaxLength(60).IsRequired();
            Property(x => x.Role).HasMaxLength(20).IsRequired();
        }
    }
}
