﻿using SalesManager.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesManager.infra.Mappings
{
    public class SalesMap : EntityTypeConfiguration<Sales>
    {
        public SalesMap()
        {
            ToTable("Sale");

            HasKey(x => x.Id);
            Property(x => x.UserId).IsRequired();
            Property(x => x.UserSeller).HasMaxLength(60).IsRequired();
            Property(x => x.Classification).HasMaxLength(60).IsRequired();
            Property(x => x.Name).HasMaxLength(60).IsRequired();
            Property(x => x.Phone).HasMaxLength(20).IsRequired();
            Property(x => x.Gender).HasMaxLength(20).IsRequired();
            Property(x => x.City).HasMaxLength(60).IsRequired();
            Property(x => x.Region).HasMaxLength(20).IsRequired();
            Property(x => x.AcquireDate).IsRequired();

            //HasRequired(x => x.User);

        }
    }
}
