﻿using SalesManager.Domain;
using SalesManager.infra.Mappings;
using System;
using System.Data.Entity;
using System.Security.Cryptography;
using System.Text;

namespace SalesManager.infra.DataContexts
{
    public class SalesManangerDataContext : DbContext
    {
        public SalesManangerDataContext() 
            : base("SalesManagerConnectionString")
        {
            Database.SetInitializer<SalesManangerDataContext>(new SalesManagerDataContextInitializer());
        }

        public DbSet<Sales> Sales { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SalesMap());
            modelBuilder.Configurations.Add(new UserMap());
            base.OnModelCreating(modelBuilder);
        }
    }
    public class SalesManagerDataContextInitializer : DropCreateDatabaseIfModelChanges<SalesManangerDataContext>
    {
        protected override void Seed(SalesManangerDataContext context)
        {
            var date = DateTime.Now;
            var passAdmin = Encrypt("admin123");
            var passSeller1 = Encrypt("seller1123");
            var passSeller2 = Encrypt("seller2123");

            context.Users.Add(new User { Id = 1, Administrator = true, Email = "admin@sellseverything.com", Password = passAdmin, Role = "Administrator", Seller = "Admin" });
            context.Users.Add(new User { Id = 2, Administrator = false, Email = "seller1@sellseverything.com", Password = passSeller1 , Role = "Seller", Seller = "Seller1" });
            context.Users.Add(new User { Id = 3, Administrator = false, Email = "seller2@sellseverything.com", Password = passSeller2, Role = "Seller", Seller = "Seller2" });
            context.SaveChanges();

            context.Sales.Add(new Sales { Id = 1, UserId = 2, City = "Porto Alegre", Region = "South", AcquireDate = date, Classification = "Vip", Gender = "Male", Name = "Peter Ipsum", Phone = "55 9 9988776655", UserSeller = "Seller1" });
            context.Sales.Add(new Sales { Id = 1, UserId = 3, City = "Porto Alegre", Region = "North", AcquireDate = date, Classification = "Regular", Gender = "Female", Name = "Maria Ipsum", Phone = "55 9 9988776654", UserSeller = "Seller2" });
            context.Sales.Add(new Sales { Id = 1, UserId = 3, City = "Porto Alegre", Region = "East", AcquireDate = date, Classification = "Sporadic", Gender = "Female", Name = "Janet Ipsum", Phone = "55 9 9988776653", UserSeller = "Seller2" });
            context.SaveChanges();

            base.Seed(context);
        }
        public string Encrypt(string value)

        {
            MD5 md5Hasher = MD5.Create();
            byte[] encryptedValue = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < encryptedValue.Length; i++)

            {
                strBuilder.Append(encryptedValue[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }
    }

}
